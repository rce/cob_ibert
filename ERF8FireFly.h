//*class ERF8FireFly
// 
// Twinax Bundle Q/C ERF8 Dual FireFly adaptor channel class   
//
// Updates:  Su Dong   Apr/25/2023 Creation
//   
#ifndef ERF8FIREFLY
#define ERF8FIREFLY
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"

class ERF8FireFly : public TObject { 

public:
 
  ERF8FireFly() {}

  virtual ~ERF8FireFly() {} 

  ERF8FireFly( int pinAdaptor, char* sigAdaptor, int PFireFly, int FFchan, char* FFpin );

  void  Print();
 
  const int PinAdaptor()     { return _pinAdaptor; }  
  const char* SignalAdaptor(){ return _signalAdaptor; }
  const int PFireFly()       { return _PFireFly; }  
  const int FireFlyChan()    { return _FireFlyChan; }  
  const char* FFchanPin()    { return _FFchanPin; }

  int FireFlyChanName( int InPconn, int InFFchan, char* OutChanName );  // covert FireFly channel to text name 
  
private: 
  
  int   _pinAdaptor;        // Adaptor ERF8 pin #
  char  _signalAdaptor[8];  // Adaptor ERF8 pin signal name 
  int   _PFireFly;          // Adaptor FireFly ECU5 socket connector P* (1,2)  
  int   _FireFlyChan;       // FireFly channel as labelled on FireFly cable header CA1-12.
                            // UEC5-09 pins for CA01-06 p/n are A2/3, 5/6, 8/9, 11/12, 14/15, 17/18
                            //                  CA07-12 p/n are B2/3, 5/6, 8/9, 11/12, 14/15, 17/18 
  char  _FFchanPin[8];      // FireFly pin name 

			    
  ClassDef(ERF8FireFly,1);

};
#endif
