// 
// Twinax Bundle Q/C ERF8-LPAM adaptor channel class   
//
// Updates:  Su Dong   Se/6/2024 Creation
//   
#include <stdio.h>
#include "ERF8Lpam.h" 
 
ClassImp(ERF8Lpam)  

//
// Load the class 
//
ERF8Lpam::ERF8Lpam( int pinAdaptor, char* sigAdaptor,
		    int JnLPAM, int LPAMch, int PP0pd, char* LPAMpn ) {
    _pinAdaptor = pinAdaptor;
    strcpy(_signalAdaptor, sigAdaptor);
    _JLPAM    = JnLPAM;
    _LPAMchan = LPAMch;
    _PP0pad   = PP0pd;
    strcpy(_LPAMpin, LPAMpn);
} 
//
// LPAM channel text name 
//
int ERF8Lpam::LPAMchanName( int InJP, int InLPAMchan, char* InLPAMpin, char* OutChanName ){
  if(InLPAMchan==20) {
   snprintf(OutChanName,8,"LPAM-J%i-CMD-%s",InJP,InLPAMpin);
  } else { 
   snprintf(OutChanName,8,"LPAM-J%i-Data%i-%s",InJP,InLPAMchan,InLPAMpin);
  }
  return 1;  
}
//
// Print one line per ERF8 pin 
//
void ERF8Lpam::Print() {
  string PP0name[5] = { "L0+L1", "L0+2L1", "C-Ring", "I-Ring", "Q-Ring" };
  printf("Adaptor ERF8 pin=%2i %s LPAM-J%i-%i PP0-CA%i\n",
	 _pinAdaptor, _signalAdaptor, _JLPAM, _LPAMchan, _PP0pad);
}
