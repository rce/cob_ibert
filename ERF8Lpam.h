//*class ERF8LPAM
// 
// Twinax Bundle Q/C ERF8 - LPAM adaptor class   
//
// Updates:  Su Dong   Sep/6/2024 Creation
//   
#ifndef ERF8LPAM
#define ERF8LPAM
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"

class ERF8Lpam : public TObject { 

public:

  ERF8Lpam() {};

  //virtual ~ERF8LPAM() {}; 

  ERF8Lpam( int pinAdaptor, char* sigAdaptor,
	   int JnLPAM, int LPAMch, int PP0pd, char* LPAMpn );

  void  Print();
 
  const int PinAdaptor()     { return _pinAdaptor; }  
  const char* SignalAdaptor(){ return _signalAdaptor; }
  const int JLPAM()          { return _JLPAM; }  
  const int LPAMchan()       { return _LPAMchan; }
  const int PP0pad()         { return _PP0pad; }  
  const char* LPAMpin()      { return _LPAMpin; }

  // convert LPAM channel to text name 
  int LPAMchanName( int InJP, int InLPAMchan, char* inLPAMpin, char* OutChanName );

  enum LPAM { L0=0, L1=1, R0=2, R05=3, Quad=4 };
	 
private: 
  
  int   _pinAdaptor;        // Adaptor ERF8 pin #
  char  _signalAdaptor[8];  // Adaptor ERF8 pin signal name 
  int   _JLPAM;             // Adaptor LPAM connector J#  
  int   _LPAMchan;          // LPAM channels data channel 0-N; 20=CMD  
  int   _PP0pad;            // PP0 twinax pad index   
  char  _LPAMpin[8];        // LPAM pin name 
			    
  ClassDef(ERF8Lpam,1);

};
#endif
