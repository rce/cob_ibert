// 
// Twinax Bundle Q/C ERF8-miniDP adaptor class   
//
// Updates:  Su Dong   Nov/23/2022 Creation
//   
#include <stdio.h>
#include "ERF8miniDP.h" 
 
ClassImp(ERF8miniDP)  

//
// Load the class 
//
ERF8miniDP::ERF8miniDP( int pinRTM, char* sigRTM, int pinAdaptor, char* sigAdaptor, int JminiDP, int miniDPchan ) {
    _pinRTM = pinRTM;
    strcpy(_signalRTM, sigRTM);
    _pinAdaptor = pinAdaptor;
    strcpy(_signalAdaptor, sigAdaptor);
    _JminiDP=JminiDP;
    _miniDPchan=miniDPchan;
} 
//
// Print one line per ERF8 pin 
//
void ERF8miniDP::Print() {
  char miniDPname[8];
  if(_miniDPchan<4) {
    snprintf(miniDPname,8,"J%i-D%i",_JminiDP,_miniDPchan);
  } else if(_miniDPchan==4) {
    snprintf(miniDPname,8,"J%i-CMD",_JminiDP);
  } else {
    snprintf(miniDPname,8,"J%i-???",_JminiDP);    
  }  
  printf("RTM ERM8 pin=%2i %8s  Adaptor ERF8 pin=%2i %8s  miniDP: %8s\n",
	 _pinRTM, _signalRTM, _pinAdaptor, _signalAdaptor, miniDPname);
}
//
// Channel map of miniDP-miniDP cable from one end to the other
//  D0<->D3, D1<->D2,  CMD<->CMD 
//
int ERF8miniDP::miniDP_cable_connectivity( int Inchan ){
  if(Inchan<4)       { return (3-Inchan); }
  else if(Inchan==4) { return 4;  }
  else               { return -1; } 
}

//
// miniDP channel text name 
//
int ERF8miniDP::miniDPchanName( int Inchan, char* OutChanName ){
  if(Inchan<4)       { snprintf(OutChanName,8,"D%i",Inchan); }
  else if(Inchan==4) { snprintf(OutChanName,8,"CMD");  }
  else               { snprintf(OutChanName,8,"???");  }
  return 1;  
}

//
// Convert miniDP map file channel text name to channel number  
//
int ERF8miniDP::miniDPchan( char* ChanName ){
  int miniDPchan = -1; 
  if(strcmp(ChanName,"CMD")==0) { miniDPchan=4; } 
  else if(strcmp(ChanName,"D0")==0) { miniDPchan=0; } 
  else if(strcmp(ChanName,"D1")==0) { miniDPchan=1; } 
  else if(strcmp(ChanName,"D2")==0) { miniDPchan=2; } 
  else if(strcmp(ChanName,"D3")==0) { miniDPchan=3; }
  return miniDPchan;   
}
