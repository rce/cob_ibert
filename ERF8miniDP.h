//*class ERF8miniDP
// 
// Twinax Bundle Q/C ERF8-miniDP adaptor class   
//
// Updates:  Su Dong   Nov/23/2022 Creation
//   
#ifndef ERF8MINIDP
#define ERF8MINIDP
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"

class ERF8miniDP : public TObject { 

public:
 
  ERF8miniDP() {}

  virtual ~ERF8miniDP() {} 

  ERF8miniDP( int pinRTM, char* sigRTM, int pinAdaptor, char* sigAdaptor, int JminiDP, int Jchan );

  void  Print();
 
  const int PinRTM()         { return _pinRTM; } 
  const char* SignalRTM()    { return _signalRTM; }
  const int PinAdaptor()     { return _pinAdaptor; }  
  const char* SignalAdaptor(){ return _signalAdaptor; }
  const int JminiDP()        { return _JminiDP; }  
  const int miniDPchan()     { return _miniDPchan; }  

  int miniDP_cable_connectivity( int InChan );          // miniDP-miniDP cable channel map between two ends 
  int miniDPchanName( int InChan, char* OutChanName );  // covert miniDP channel to text name 
  int miniDPchan( char* ChanName );                     // convert map file channel name to channel number    
  
private: 
  
  int   _pinRTM;            // RTM ERM8 connector pin #
  char  _signalRTM[8];      // RTM ERM8 pin signal name
  int   _pinAdaptor;        // Adaptor ERF8 pin #
  char  _signalAdaptor[8];  // Adaptor ERF8 pin signal name 
  int   _JminiDP;           // Adaptor miniDP connector number J*
  int   _miniDPchan;        // miniDP channel 0-3=Data; 4=CMD
  
  ClassDef(ERF8miniDP,1);

};
#endif
