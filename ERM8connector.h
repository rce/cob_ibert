//*class ERM8connector
// 
// Twinax Bundle Q/C COB RTM ERM8 connector interface class   
//
// Updates:  Su Dong   Nov/23/2022 Creation
//   
#ifndef ERM8CONN
#define ERM8CONN
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"

class ERM8connector : public TObject { 

public:
 
  ERM8connector() {}

  virtual ~ERM8connector() {} 

  ERM8connector( int pin, int sigType, char* sigName, int indRCE, int Mbank, int Mchan );

  void  Print();
 
  const int Pin()            { return _pin; } 
  const int SignalType()     { return _signalType; } 
  const char* SignalName()   { return _signalName; }
  const int RCE()            { return _RCE; }
  const int MGTbank()        { return _MGTbank; }  
  const int MGTchan()        { return _MGTchan; }  

  // DPM TX/RX direction reversed ?  (0,1=Normal; 2,3=reverse) 
  const int DPMdirection( int indDPM ) {
    if(indDPM<=1){ return Normal; }
    else { return Reverse; }
  } 

  // Pin signal TX/RX direction including DPM direction reversal   
  const int SignalDirection( int indDPM );

  // Signal direction name
  const int SigDirName( int indDPM, char* dirName ); 
  
  // MGT channel Y in iBERT 
  const int MGT_Y() { return _MGTchan + 4*(_MGTbank-109); }

  // Inverted ERF8 pin this ERM8 pin would mate to
  const int InvertedERF8pin();
  
  enum { NC=0, TX=1, RX=2 };      // RTM schematic signal type
  enum { Normal=0, Reverse=1 };   // DPM signal group direction 
   
private: 
  
  int   _pin;            // ERM8 connector pin #  
  int   _signalType;     // Pin signal type for normal DPM direction (NC=0, TX=1, RX=2)
  char  _signalName[8];  // ERM8 pin signal name
  int   _RCE;            // which RCE within DPM
  int   _MGTbank;        // ZYNQ MGT bank number
  int   _MGTchan;        // MGT channel within a bank 
  
  ClassDef(ERM8connector,1);

};
#endif
