//*class FFconfig
// 
// Twinax Bundle Q/C FireFly bundle setup configuration class   
//
// Updates:  Su Dong   Apr/16/2024 initial version 
//
#ifndef FFCONFIG
#define FFCONFIG
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"
#include <TFile.h>

const int MaxCable = 32;  // Max number of twinax (one full optoboard)  
const int MaxFF = 2;      // Max number of Firefly per bundle  

class FFconfig : public TObject { 

public:
 
  FFconfig() {}

  virtual ~FFconfig() {} 
  
  void  Print();
  int   ReadConfig(const string confFile);  // Read in config info from a file  
  int   TCLheading(FILE* TCLfile);     // Add Config info to TCL file as header commenta  
 
  const char* ConfigTitle()  { return _configTitle; } 
  const char* Location()     { return _location; }
  const char* iBERTbitFile() { return _iBERTbitFile; } 
  const char* TCLfileLink()  { return _TCLfileLink; } 
  const char* TCLfileScan()  { return _TCLfileScan; }
  const char* CSVfileMap()   { return _CSVfileMap; }
  const int   ERMgroupFF()   { return _ERMgroupFF;}   
  const int   ERMgroupOpto() { return _ERMgroupOpto;}   
  const uint64_t JtagIDFF()    { return _JtagIDFF; }    
  const uint64_t JtagIDopto()  { return _JtagIDopto; }
  const int   ERFinvert()    { return _ERFinvert;}   
  const int   OptoTerm()     { return _optoTerm; }
  const int   OptoFlavor()   { return _optoTerm/10;} 
  const int   OptoTopo()     { return _optoTerm%10;} 
  const int   Nfirefly()     { return _nFF; }  
  const int   FFport(int indF)      { return _FFport[indF]; }  
  const int   Ncable()       { return _nCable; }  
  const int   CableFFconn(int indC) { return _cableFFconn[indC]; }   
  const int   CableFFchan(int indC) { return _cableFFchan[indC]; }   
  const int   CableOptoPad(int indC)  { return _cableOptoPad[indC]; }   
  const int   CableCross(int indC)  { return _cableCross[indC]; }   

  enum { Slim=0, Lshape=1 }; 
  enum { Normal=0, Mirrored=1 }; 
    
private: 
  
  char     _configTitle[96];    // Configuration title
  char     _location[64];       // Test setup location   
  char     _iBERTbitFile[96];   // iBERT firmware bit file     
  char     _TCLfileLink[96];    // Output TCL file for link declarations   
  char     _TCLfileScan[96];    // Output TCL file for eye scans    
  char     _CSVfileMap[96];     // Output CSV file for connectivity map    
  int      _ERMgroupFF;         // RTM/DPM ERM8 group number for FF adaptor;
  int      _ERMgroupOpto;       // RTM/DPM ERM8 group number for OptoTerm      
  uint64_t _JtagIDFF;           // Jtag ID of the FF ERM group for Vivado  
  uint64_t _JtagIDopto;         // Jtag ID of the FF ERM group for Vivado
  int      _ERFinvert;          // Detector FE side adaptor ERF8 inverted connection: 1=inverted    
  int      _optoTerm;           // OptoTerm type =10*SL+NM  SL=0/1=Slim/Lshape; NM=0/1=Norm/Mirror
  int      _nFF;                // number of Fireflys in the bundle
  int      _FFport[MaxFF];      // Dual FF-ERF8 adaptor port P* number(1,2) a FFis plugged into. 0=N/C.   
  int      _nCable;             // Number of twinax cables in connected bundle  
  int      _cableFFconn[MaxCable];  // cable inner end FireFly number   
  int      _cableFFchan[MaxCable];  // cable inner end FireFly twinax pad CA number
  int      _cableOptoPad[MaxCable]; // cable outer end OptoTerm pad CA number   
  int      _cableCross[MaxCable];   // cable FF-Opto path +/- polarity 0=no crossover / 1=crossover outer 
  
  ClassDef(FFconfig,1);

};
#endif
