// 
// Twinax Bundle Q/C LPAM-ERM bundle setup configuration class   
//
// Updates:  Su Dong   Sep/6/2024 Initial version 
//   
#include <stdio.h>
#include "LPAMconfig.h" 
 
ClassImp(LPAMconfig)  
//
// Print configuration 
//
void LPAMconfig::Print() {
  printf("Title: %s\n",_configTitle); 
  printf("Location: %s\n",_location);
  printf("iBERT bit file: %s\n",_iBERTbitFile);
  printf("Output Link TCL: %s\n",_TCLfileLink);
  printf("Output Scan TCL: %s\n",_TCLfileScan);
  printf("Output Map CSV:  %s\n",_CSVfileMap);
  printf("LPAM adaptor ERM group %i  JtagID=0x%" PRIX64 "\n", _ERMgroupLPAM,_JtagIDLPAM);
  printf("OptoTerm     ERM group %i  JtagID=0x%" PRIX64 "\n", _ERMgroupOpto,_JtagIDopto);
  printf("Detector FE adaptor ERF8 connector: ");
  if(_ERFinvert!=0) { printf("Inverted\n"); }
  else { printf("Normal\n"); }
  printf("OptoTerm Flavor: ");
  if(OptoFlavor()==Slim) { printf("Slim-"); } 
  else if(OptoFlavor()==Lshape) { printf("Lshape-"); } 
  else { printf("%i-",OptoFlavor()); }
  if(OptoTopo()==Normal) { printf("Normal\n"); } 
  else if(OptoTopo()==Mirrored) { printf("Mirrored\n"); } 
  else { printf("%i\n",OptoTopo()); }
  string PP0name[5] = { "L0+L1", "L0+2L1", "C-Ring", "I-Ring", "Q-Ring" };   
  if(_PP0type>=0 && _PP0type<5) {
    printf("PP0 type = %i (%s)\n",_PP0type,PP0name[_PP0type].c_str());  
  } else {
    printf("PP0 type = %i (Unknown)\n",_PP0type);
  }
  printf("No. of LPAMs = %i. Adaptor ports: ",NLPAM());
  for(int i=0;i<NLPAM();i++) { printf("%i ", LPAMport(i));} 
  printf("\n"); 
  printf("%i twinax cables:\n",_nCable);
  for(int indC=0; indC<_nCable; indC++) {
    printf("LPAM%i-%i PP0-CA%i OptoTerm pad CA%i",
	   _cableLPAMconn[indC],_cableLPAMchan[indC],_cablePP0pad[indC],_cableOptoPad[indC]);
    if(_cableCross[indC]==0) { printf("+-\n"); }
    else                     { printf("-+\n"); }
  }
}
//
// Add config info into TCL header comment 
//
int LPAMconfig::TCLheading(FILE* TCLfile) {
  fprintf(TCLfile,"# %s\n",_configTitle); 
  fprintf(TCLfile,"# Location: %s\n",_location);
  fprintf(TCLfile,"# LPAM adaptor ERM group %i  JtagID=0x%" PRIX64 "\n", _ERMgroupLPAM,_JtagIDLPAM);
  fprintf(TCLfile,"# Optoterm     ERM group %i  JtagID=0x%" PRIX64 "\n", _ERMgroupOpto,_JtagIDopto);
  fprintf(TCLfile,"# %i twinax cables: ",_nCable);
  return 1; 
}

//
// Read in configuration from a file 
//
int LPAMconfig::ReadConfig(const string confFile) {

  bool DBGmap = 0;    // debug flag for pinting details  
  
  ifstream configFile(confFile.c_str());
  string line(120,' ');
  if(!configFile.is_open()) { 
    cout << "Error in opening RTM connectivity map file: " << confFile << endl;
    return 0;
  } else {
    cout << "Initialization from configuration file: " << confFile << endl;    
  }
  int numline = 0;
  int ind  = 0;
  int nGroup = 0;
  _nCable = 0;
  strcpy(_TCLfileLink,"");
  strcpy(_TCLfileScan,"");
  strcpy(_CSVfileMap,"");

  
  // Read the first line (item names) 
  getline (configFile,line);
  if(DBGmap) cout << line << endl;
  numline++;

  // Loop over config file lines  
  while (! configFile.eof() )
  {
    getline (configFile,line);
    numline++;
    if(DBGmap) cout << line << endl; 
    //
    // Parse lines into parameters
    //
    if(line!=""){ 
      std::size_t hash = line.find("#");
      if(hash==std::string::npos) {   // skip comment lines with #     
        std::size_t colon = line.find(":"); 
	if(colon!=std::string::npos) { // must have a : to define parameter      
	  std::string parmname   = line.substr(0,colon);  // parameter name before : 
   	  std::string parmvalraw = line.substr(colon+1,line.length()); // parameter values after :
	  // Trim out leading space to param value string to start from first non-space character
	  std::wstring::size_type first_non_space_pos = parmvalraw.find_first_not_of(' ');
   	  std::string parmvalues = parmvalraw.substr(first_non_space_pos,parmvalraw.length());   
	  if(parmname.find("title")!=std::string::npos) {
	    strcpy(_configTitle,parmvalues.c_str());
	  } else if(parmname.find("location")!=std::string::npos) {      
	    strcpy(_location,parmvalues.c_str());
	  } else if(parmname.find("iBERTbitFile")!=std::string::npos) {      
	    strcpy(_iBERTbitFile,parmvalues.c_str());
          } else if(parmname.find("TCLfileLink")!=std::string::npos) {      
            cout << "TCLlink: " << parmvalues << endl;   
	    strcpy(_TCLfileLink,parmvalues.c_str());
	  } else if(parmname.find("TCLfileScan")!=std::string::npos) {      
	    strcpy(_TCLfileScan,parmvalues.c_str());
	  } else if(parmname.find("CSVfileMap")!=std::string::npos) {      
	    strcpy(_CSVfileMap,parmvalues.c_str());
          } else if(parmname.find("ERMgroup-LPAM")!=std::string::npos) {      
            sscanf(parmvalues.c_str(),"%i,%" PRIX64 ,&_ERMgroupLPAM,&_JtagIDLPAM);
            nGroup++;
	  } else if(parmname.find("ERMgroup-opto")!=std::string::npos) {
            sscanf(parmvalues.c_str(),"%i,%" PRIX64 ,&_ERMgroupOpto,&_JtagIDopto);
            nGroup++;
	  } else if(parmname.find("ERFinvert")!=std::string::npos) {      
	    sscanf(parmvalues.c_str(),"%i", &_ERFinvert); 
       //
       // OptoTerm and PP0 LPAM configurations 
       //
	  } else if(parmname.find("PP0type")!=std::string::npos) {      
	  sscanf(parmvalues.c_str(),"%i", &_PP0type); 
	  } else if(parmname.find("OptoFlavor")!=std::string::npos) {      
	  sscanf(parmvalues.c_str(),"%i", &_optoTerm); 
	  } else if(parmname.find("LPAM")!=std::string::npos) {      
	  sscanf(parmvalues.c_str(),"%i,%i,%i", &_nLPAM, &_LPAMport[0], &_LPAMport[1]); 
	//
	// cable lines are specified by 5 numbers: LPAM Jn, LPAM chan, PP0pad (CA), OptoTerm pad (CA), PolFlip 
	//
	  } else if(parmname.find("Cable")!=std::string::npos) {      
	    if(_nCable<MaxCable) {
              sscanf(parmvalues.c_str(),"%i,%i,%i,%i,%i",
		     &_cableLPAMconn[_nCable],&_cableLPAMchan[_nCable],&_cablePP0pad[_nCable],
		     &_cableOptoPad[_nCable],&_cableCross[_nCable]);
              _nCable++;
            } else {
	      cout << "ERROR: too many cables: " << line << endl;
	      return 0; 
	    }
	  } else { 
	    cout << "ERROR: unknown param: " << parmname << endl;
	    return 0; 
	  }	  
        } else {
	  cout << "WARNING: Config line skipped due to missing ':' in " << line << endl;      
	}
      }
    }  
  }

  configFile.close();
  cout << "LPAM config file " << confFile << " read with " << numline << " lines" << endl;  
  LPAMconfig::Print();
  if(nGroup!=2) {
    cout << "ERROR: incorrect number of specified ERMgroups = " << nGroup << endl;    
    return 0;
  }

  return 1; 
}
