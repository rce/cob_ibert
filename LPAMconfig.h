//*class LPAMconfig
// 
// Twinax Bundle Q/C IS PP0 LPAM bundle setup configuration class   
//
// Updates:  Su Dong   Sep/6/2024 initial version 
//
#ifndef LPAMCONFIG
#define LPAMCONFIG
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"
#include <TFile.h>

const int MaxCable = 32;  // Max number of twinax (one full optoboard)  
const int MaxLPAM  = 2;   // Max number of LPAM per bundle  

class LPAMconfig : public TObject { 

public:
 
  LPAMconfig() {}

  virtual ~LPAMconfig() {} 
  
  void  Print();
  int   ReadConfig(const string confFile);  // Read in config info from a file  
  int   TCLheading(FILE* TCLfile);     // Add Config info to TCL file as header commenta  
 
  const char* ConfigTitle()  { return _configTitle; } 
  const char* Location()     { return _location; }
  const char* iBERTbitFile() { return _iBERTbitFile; } 
  const char* TCLfileLink()  { return _TCLfileLink; } 
  const char* TCLfileScan()  { return _TCLfileScan; }
  const char* CSVfileMap()   { return _CSVfileMap; }
  const int   ERMgroupLPAM() { return _ERMgroupLPAM;}   
  const int   ERMgroupOpto() { return _ERMgroupOpto;}   
  const uint64_t JtagIDLPAM() { return _JtagIDLPAM; }    
  const uint64_t JtagIDopto()  { return _JtagIDopto; }
  const int   ERFinvert()    { return _ERFinvert;}   
  const int   OptoTerm()     { return _optoTerm; }
  const int   OptoFlavor()   { return _optoTerm/10;} 
  const int   OptoTopo()     { return _optoTerm%10;} 
  const int   PP0type()      { return _PP0type; }  
  const int   NLPAM()        { return _nLPAM; }  
  const int   LPAMport(int indP)      { return _LPAMport[indP]; }  
  const int   Ncable()       { return _nCable; }  
  const int   CableLPAMconn(int indC) { return _cableLPAMconn[indC]; }   
  const int   CableLPAMchan(int indC) { return _cableLPAMchan[indC]; }   
  const int   CablePP0pad(int indC)  { return _cablePP0pad[indC]; }   
  const int   CableOptoPad(int indC)  { return _cableOptoPad[indC]; }   
  const int   CableCross(int indC)  { return _cableCross[indC]; }

  enum { Slim=0, Lshape=1 }; 
  enum { Normal=0, Mirrored=1 };
  enum PP0 { L0L1=0, L02L1=1, CR=2, IR=3, QR=4 };

    
private: 
  
  char     _configTitle[96];    // Configuration title
  char     _location[64];       // Test setup location   
  char     _iBERTbitFile[96];   // iBERT firmware bit file     
  char     _TCLfileLink[96];    // Output TCL file for link declarations   
  char     _TCLfileScan[96];    // Output TCL file for eye scans    
  char     _CSVfileMap[96];     // Output CSV file for connectivity map    
  int      _ERMgroupLPAM;       // RTM/DPM ERM8 group number for PP0/LPAM adaptor;
  int      _ERMgroupOpto;       // RTM/DPM ERM8 group number for OptoTerm      
  uint64_t _JtagIDLPAM;         // Jtag ID of the Lpam ERM group for Vivado  
  uint64_t _JtagIDopto;         // Jtag ID of the Opto ERM group for Vivado
  int      _ERFinvert;          // Detector FE side adaptor ERF8 inverted connection: 1=inverted    
  int      _optoTerm;           // OptoTerm type =10*SL+NM  SL=0/1=Slim/Lshape; NM=0/1=Norm/Mirror
  int      _PP0type;            // PP0 type 
  int      _nLPAM;              // number of Fireflys in the bundle
  int      _LPAMport[MaxLPAM];  // LPAM-ERF8 adaptor port J* number(1,2) a LPAM Type-0 is plugged into. 0=N/C.   
  int      _nCable;             // Number of twinax cables in connected bundle  
  int      _cableLPAMconn[MaxCable];  // cable inner end LPAM number   
  int      _cableLPAMchan[MaxCable];  // cable inner end LPAM channel number 
  int      _cablePP0pad[MaxCable];    // cable inner end PP0 twinax pad CA number
  int      _cableOptoPad[MaxCable];   // cable outer end OptoTerm pad CA number   
  int      _cableCross[MaxCable];     // cable PP0-Opto path +/- polarity 0=no crossover / 1=crossover outer 
  
  ClassDef(LPAMconfig,1);

};
#endif
