//*class OptoTerm
// 
// Twinax Bundle Q/C Opto terminaion flex PCB class   
//
// Updates:  Su Dong   Apr/16/2024 Initial version 
//   
#ifndef OPTOTERM
#define OPTOTERM
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>
#include "OptoTermPin.h"

#include "TObject.h"

const int MaxPin = 64;
 
class OptoTerm : public TObject { 

public:
 
  OptoTerm() {} 

  virtual ~OptoTerm() {}
  
  void  Print();
 
  const int OptoType()       { return _OptoType; } 
  const int Npins()          { return _nPin; }  
  OptoTermPin* getPadPin(int indP) { return &_pinList[indP]; }
  int findPadPinPlus(int padNum);  // Get pad pin+ index for a given twinax pad number   
  int findPadPinMinus(int padNum); // Get pad pin- index for a given twinax pad number
  int findPadPinERF8(int pinERF8); // Get pad pin index for a given ERF8 pin
  
  int OptoMapInit(int oType); // read in .csv map file defines the optoTerm connectivity of pads and pins    

  enum { Slim=0, Lshape=1 }; 
  enum { Normal=0, Mirrored=1 }; 
  
private: 
  
  int   _OptoType;              // Type of OptoTerm board = 10*SL+NM  SL=0/1=Slim/Lshape; NM=0/1=Normal/Mirrored   
  int   _nPin;                  // No. of active ERF8 pins / twinax wires 
  OptoTermPin _pinList[MaxPin]; // ERF8 pin / twinax wire array 
			    
  ClassDef(OptoTerm,1);

};
#endif
