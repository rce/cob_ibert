//*class OptoTermPin
// 
// Twinax Bundle Q/C Opto termination board signal trace between ERF8 connector and twinax soldering pads
//
// Updates:  Su Dong   Apr/16/2024 Initial version 
//   
#ifndef OPTOTERMPIN
#define OPTOTERMPIN
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"

class OptoTermPin : public TObject { 

public:
 
  OptoTermPin() {}

  virtual ~OptoTermPin() {}

  OptoTermPin( int pinERF8, char* sigERF8, int twxPad, int twxWire );

  void  Print();

  const int PinERF8()        { return _pinERF8; } 
  const char* SignalERF8()   { return _signalERF8; }
  const int CablePad()       { return _cablePad; }  
  const int CableWire()      { return _cableWire; }

  int SignalPolarity(); // Check signal name for polarity type

  enum { pinUnknown=0, pinPlus=1, pinMinus=2 };  
  
private: 
  
  int   _pinERF8;            // ERF8 connector pin #
  char  _signalERF8[8];      // ERF8 pin signal name
  int   _cablePad;           // Twinax pads labelled as CA1-32.
  int   _cableWire;          // Which wire of twinax 1,2 connected to this leg of twinax pad   
			    
  ClassDef(OptoTermPin,1);

};
#endif
