//
// Utilility routine to read in the RTM-DPM-MGT connectivity map from a file    
//
#include <TFile.h>
#include "ERM8connector.h"

int RTMmapInit( int &Npairs, ERM8connector RTMpin[] ) { 
 
  bool DBGmap = 0;    // debug flag for pinting details  
  
  const string filename = "map-RTM-DPM-MGT.csv";
  ifstream mapfile(filename.c_str());
  string line(120,' ');
  if(!mapfile.is_open()) { 
    cout << "Error in opening RTM connectivity map file: " << filename << endl;
    return 0;
  } else {
    cout << "Initializetion RTM/DPM/MGT map file: " << filename << endl;    
  }
  int numline = 0;
  int ind  = 0;

  // Read the first line (item names) 
  getline (mapfile,line);
  if(DBGmap) cout << line << endl;
  numline++;

  // Loop over map file lines  
  while (! mapfile.eof() )
  {
    getline (mapfile,line);
    numline++;
    if(DBGmap) cout << line <<endl;
    //
    // Unack lines with MGT channel designation 
    //
    if(line!=""){ 
      int pin, sigInd, indRCE, MGTbank, MGTchan, MGTy;
      char sigName[16];
      char sigDir[16];
      strcpy(sigDir,"");
      sscanf(line.c_str(),"%i,%[^','],%[^',']",&pin,sigName,sigDir);
      if(DBGmap) cout << "sigName=" << sigName << "  sigDir=" << sigDir << endl; 
      if(strcmp(sigDir,"TX")==0 || strcmp(sigDir,"RX")==0 ) {  
        sscanf(line.c_str(),"%i,%[^','],%[^','],%i,%i,%i,%i,%i",&pin,sigName,sigDir,&sigInd,
	     &indRCE,&MGTbank,&MGTchan,&MGTy);
        if(strcmp(sigDir,"TX")==0) {
   	  RTMpin[ind++] = ERM8connector(pin, ERM8connector::TX, sigName, indRCE, MGTbank, MGTchan);  
        } else if(strcmp(sigDir,"RX")==0) {
	  RTMpin[ind++] = ERM8connector(pin, ERM8connector::RX, sigName, indRCE, MGTbank, MGTchan);
	}
      }
    }  
  }

  mapfile.close();
  printf("Lines read = %i\n",numline);
  printf("TX/RX map pairs = %i\n",ind);

  Npairs = ind;
  if(DBGmap) { 
    for(int i=0;i<Npairs;i++) {
      printf("%3i ",i); 
      RTMpin[i].Print();
    }
  }
  cout << "Npairs=" << Npairs << endl; 
  
  return 1; 
}
