#-----------------------------------------------------------
# Vivado v2022.1 (64-bit)
# Generated with genTCL for SLAC EPP lab setup
# All 6 miniDP connected Jn<->Jn  
#-----------------------------------------------------------
start_gui
open_hw_manager
set_param labtools.enable_multiple_cables 1
connect_hw_server -url localhost:3121 -allow_non_jtag
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210299B387DF]
set_property PARAM.FREQUENCY 15000000 [get_hw_targets */xilinx_tcf/Digilent/210299B387DF]
open_hw_target
current_hw_device [get_hw_devices xc7z045_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z045_1] 0]
current_hw_device [get_hw_devices xc7z045_3]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z045_3] 0]
open_hw_target {localhost:3121/xilinx_tcf/Digilent/210299B3949D}
current_hw_device [get_hw_devices xc7z045_1_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z045_1_1] 0]
current_hw_device [get_hw_devices xc7z045_3_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z045_3_1] 0]
set_property PROBES.FILE {} [get_hw_devices xc7z045_1]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z045_1]
set_property PROGRAM.FILE {/home/slacrce/ibert3tile.bit} [get_hw_devices xc7z045_1]
program_hw_devices [get_hw_devices xc7z045_1]
refresh_hw_device [lindex [get_hw_devices xc7z045_1] 0]
set_property PROBES.FILE {} [get_hw_devices xc7z045_3]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z045_3]
set_property PROGRAM.FILE {/home/slacrce/ibert3tile.bit} [get_hw_devices xc7z045_3]
program_hw_devices [get_hw_devices xc7z045_3]
refresh_hw_device [lindex [get_hw_devices xc7z045_3] 0]
set_property PROBES.FILE {} [get_hw_devices xc7z045_1_1]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z045_1_1]
set_property PROGRAM.FILE {/home/slacrce/ibert3tile.bit} [get_hw_devices xc7z045_1_1]
program_hw_devices [get_hw_devices xc7z045_1_1]
refresh_hw_device [lindex [get_hw_devices xc7z045_1_1] 0]
set_property PROBES.FILE {} [get_hw_devices xc7z045_3_1]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z045_3_1]
set_property PROGRAM.FILE {/home/slacrce/ibert3tile.bit} [get_hw_devices xc7z045_3_1]
program_hw_devices [get_hw_devices xc7z045_3_1]
refresh_hw_device [lindex [get_hw_devices xc7z045_3_1] 0]
set xil_newLinks [list]
set xil_newLink [create_hw_sio_link -description {1J1CMD-3J1CMD} [lindex [get_hw_sio_txs localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX] 0] [lindex [get_hw_sio_rxs localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX] 0] ]
lappend xil_newLinks $xil_newLink
set xil_newLink [create_hw_sio_link -description {3J1D3-1J1D0} [lindex [get_hw_sio_txs localhost:3121/xilinx_tcf/Digilent/210299B3949D/1_1_0/IBERT/Quad_111/MGT_X0Y9/TX] 0] [lindex [get_hw_sio_rxs localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_111/MGT_X0Y9/RX] 0] ]
lappend xil_newLinks $xil_newLink
set xil_newLink [create_hw_sio_link -description {3J1D2-1J1D1} [lindex [get_hw_sio_txs localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX] 0] [lindex [get_hw_sio_rxs localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y7/RX] 0] ]
lappend xil_newLinks $xil_newLink
set xil_newLink [create_hw_sio_link -description {3J1D1-1J1D2} [lindex [get_hw_sio_txs localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y7/TX] 0] [lindex [get_hw_sio_rxs localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX] 0] ]
lappend xil_newLinks $xil_newLink
set xil_newLink [create_hw_sio_link -description {3J1D0-1J1D3} [lindex [get_hw_sio_txs localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_111/MGT_X0Y9/TX] 0] [lindex [get_hw_sio_rxs localhost:3121/xilinx_tcf/Digilent/210299B387DF/1_1_0/IBERT/Quad_111/MGT_X0Y9/RX] 0] ]
lappend xil_newLinks $xil_newLink
set xil_newLinkGroup [create_hw_sio_linkgroup -description {Link Group 0} [get_hw_sio_links $xil_newLinks]]
unset xil_newLinks
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/1_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_111/MGT_X0Y9/RX}]
commit_hw_sio -non_blocking [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/1_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_111/MGT_X0Y9/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y7/RX}]
commit_hw_sio -non_blocking [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y7/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y7/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX}]
commit_hw_sio -non_blocking [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y7/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX}]
set_property PORT.RXPOLARITY {1} [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/1_1_0/IBERT/Quad_111/MGT_X0Y9/RX}]
commit_hw_sio -non_blocking [get_hw_sio_links {localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/1_1_0/IBERT/Quad_111/MGT_X0Y9/RX}]
set_property LOGIC.MGT_ERRCNT_RESET_CTRL 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
commit_hw_sio -non_blocking [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
set_property LOGIC.MGT_ERRCNT_RESET_CTRL 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
commit_hw_sio -non_blocking [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
set xil_newScan [create_hw_sio_scan -description {Scan_1J1CMD-3J1CMD} 2d_full_eye  [lindex [get_hw_sio_links localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX->localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX] 0] ]
run_hw_sio_scan [get_hw_sio_scans $xil_newScan]
wait_on_hw_sio_scan [get_hw_sio_scans $xil_newScan]
write_hw_sio_scan -force "scan_data/temp/Scan_1J1CMD-3J1CMD.csv" [get_hw_sio_scans $xil_newScan]
set xil_newScan [create_hw_sio_scan -description {Scan_3J1D3-1J1D0} 2d_full_eye  [lindex [get_hw_sio_links localhost:3121/xilinx_tcf/Digilent/210299B3949D/1_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_111/MGT_X0Y9/RX] 0] ]
run_hw_sio_scan [get_hw_sio_scans $xil_newScan]
wait_on_hw_sio_scan [get_hw_sio_scans $xil_newScan]
write_hw_sio_scan -force "scan_data/temp/Scan_3J1D3-1J1D0.csv" [get_hw_sio_scans $xil_newScan]
set xil_newScan [create_hw_sio_scan -description {Scan_3J1D2-1J1D1} 2d_full_eye  [lindex [get_hw_sio_links localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y5/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y7/RX] 0] ]
run_hw_sio_scan [get_hw_sio_scans $xil_newScan]
wait_on_hw_sio_scan [get_hw_sio_scans $xil_newScan]
write_hw_sio_scan -force "scan_data/temp/Scan_3J1D2-1J1D1.csv" [get_hw_sio_scans $xil_newScan]
set xil_newScan [create_hw_sio_scan -description {Scan_3J1D1-1J1D2} 2d_full_eye  [lindex [get_hw_sio_links localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_110/MGT_X0Y7/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/3_1_0/IBERT/Quad_110/MGT_X0Y5/RX] 0] ]
run_hw_sio_scan [get_hw_sio_scans $xil_newScan]
wait_on_hw_sio_scan [get_hw_sio_scans $xil_newScan]
write_hw_sio_scan -force "scan_data/temp/Scan_3J1D1-1J1D2.csv" [get_hw_sio_scans $xil_newScan]
set xil_newScan [create_hw_sio_scan -description {Scan_3J1D0-1J1D3} 2d_full_eye  [lindex [get_hw_sio_links localhost:3121/xilinx_tcf/Digilent/210299B3949D/3_1_0/IBERT/Quad_111/MGT_X0Y9/TX->localhost:3121/xilinx_tcf/Digilent/210299B387DF/1_1_0/IBERT/Quad_111/MGT_X0Y9/RX] 0] ]
run_hw_sio_scan [get_hw_sio_scans $xil_newScan]
wait_on_hw_sio_scan [get_hw_sio_scans $xil_newScan]
write_hw_sio_scan -force "scan_data/temp/Scan_3J1D0-1J1D3.csv" [get_hw_sio_scans $xil_newScan]
