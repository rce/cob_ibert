//*class miniDPconfig
// 
// Twinax Bundle Q/C miniDP calibration setup configuraiton class   
//
// Updates:  Su Dong   Jun/27/2023 Creation
//   
#ifndef MINIDPCONFIG
#define MINIDPCONFIG
//---------------
// C++ Headers --
//---------------
#include <stdio.h>
#include <iostream>

#include "TObject.h"
#include <TFile.h>

class miniDPconfig : public TObject { 

public:
 
  miniDPconfig() {}

  virtual ~miniDPconfig() {} 

  miniDPconfig( char* confTitle, char* setupLoc, int ERMgrp[2], uint64_t Jtag[2], int ncable, int CableMap[6][2] );

  void  Print();
  int   ReadConfig(const string confFile);  // Read in config info from a file  
  int   TCLheading(FILE* TCLfile);     // Add Config info to TCL file as header commenta  
 
  const char* ConfigTitle()  { return _configTitle; } 
  const char* Location()     { return _location; }
  const char* iBERTbitFile() { return _iBERTbitFile; } 
  const char* TCLfileLink()  { return _TCLfileLink; } 
  const char* TCLfileScan()  { return _TCLfileScan; }
  const char* CSVfileMap()   { return _CSVfileMap; }
  const int   ERMgroup(int indG) { return _ERMgroup[indG];}   
  const uint64_t JtagID(int indG) { return _groupJtag[indG]; }    
  const int   Ncable()       { return _nCable; }  
  const int   CableMiniDP(int indC, int indG) { return _cableJminiDP[indC][indG]; }   

  const int MaxMiniDP = 6;  // Number of miniDP connectors on ERF8-miniDP adaptor
		   
private: 
  
  char     _configTitle[96];    // Configuration title
  char     _location[64];       // Test setup location   
  char     _iBERTbitFile[96];   // iBERT firmware bit file     
  char     _TCLfileLink[96];    // Output TCL file for link declarations   
  char     _TCLfileScan[96];    // Output TCL file for eye scans    
  char     _CSVfileMap[96];     // Output CSV file for connectivity map    
  int      _ERMgroup[2];        // RTM/DPM ERM8 group numbers for two ERF8-miniDP adaptors   
  uint64_t  _groupJtag[2];      // ERM8 group J-tag ID for Vivado
  int      _nCable;             // Number of miniDP cables connected 
  int      _cableJminiDP[6][2];  // ERF8-miniDP adaptor miniDP J# at two ends of miniDP cables 
  
  ClassDef(miniDPconfig,1);

};
#endif
